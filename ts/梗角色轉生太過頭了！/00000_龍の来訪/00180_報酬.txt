在靠近河畔的森林中，被平整得無法相信的地面放寬了。

縱橫二十米的正方形被即使在上面建設建築物也沒關係那樣的精心整齊了。
令人吃驚的是作業需要的時間。
僅僅兩秒。
這是娜哈特的土系統魔法造成的結果。

「雖然是略微覺得，但你實在是規格外呢⋯⋯真的是人類？」

對說出了極其失禮的話的克麗絲塔，娜哈特也只有苦笑了。

「一半是，吶。而且，即使是人這種程度也是可能的。以前我有個友人是人的魔術師（Wizards），他會用地形破壊魔法轟飛山脈，而且曾將副本BOSS作為對手過哦？」
「⋯⋯⋯⋯那才不是人啊⋯⋯」

這是克麗絲塔自身被別人說過好多次的話，不過對別人說這是第一次。

「嘛，而且要吃驚還早」

那樣說後，娜哈特從道具倉庫取出了像寶石一樣的玉。
名為簡易帳篷作成玉。那是在高級店賣的道具，是不消耗被FP等代表的疲勞值度過一夜的便利系道具。

娜哈特一把那個投向空中，就升起了煙，出現了大大的圓形帳篷。
像游牧民的蒙古包一樣，結構堅固一目了然。
進入裡面的話，越發明白那有多厲害。
地板是豪奢的地毯，有兩張木製的床，七個褥子，冰鎮的水果和倒在玻璃杯裡的果汁在像被爐一樣的小桌子上閃耀著。
如果被說住的話，只要不是貴族就會回答請務必讓我住那樣的豪奢，保溫能力也很出色。

「『『哈！？』』」

當然，誰都隱藏不住驚訝。
元性奴隷的女性像在做夢一樣呆住了，一流冒険者也不禁恍惚了。
如果是莎夏和克麗絲塔等使用魔法的人看的話，一瞬間就能理解那不是由魔法產生出的東西。
娜哈特隨隨便便用了的兩顆寶珠，很容易就能想像到光那個就價值不菲。王族或貴族在拍賣會上肯定會給出無法想像的價格。

「順便一提這個是一次性的」
「『『哈！？』』」

那，那麼，請務必讓我住啦，請務必讓我剝下地毯啦，各種各樣的熱鬧的聲音交錯亂飛。
留下一句別吵啦後，娜哈特再次將手伸入倉庫。

「艾夏，我們是這邊」

剛才的只不過是一次性道具。
娜哈特擁有和那個不同的名為移動房屋的可以搬運的據點系道具。這是等級達到七十以上的玩家能無條件承接的共通任務的報酬。這個移動房屋稍微有些特殊，可以通過課金讓內部和設計變化。

娜哈特的據點是潛入小小的入口後裡面是寬敞的異次元空間，天空是點綴著顏色鮮艷的星星的夜空的房間。只靠幻想的星星的光亮，周圍散發著有些暗淡的氣氛但那就可以了。球狀的空間就像是關入了宇宙的一部分的地方。

透明的地板不僅很溫暖，而且光坐著就有MP回復量增加的效果，沙發有減輕疲勞的效果，有能收納素材的倉庫，有存錢的金庫，托了重課金的福方方面面很充實。

三次轉職和入手這個移動房屋的任務是到達七十級以後要最先承接的任務。

娜哈特帶領艾夏進入移動房屋後，催促招入裡面的克麗絲塔坐在座位上。

「你究竟什麼人？雖然這樣說不太好，但即使說是神我好像也會認可⋯⋯」

進入了幻想的空間的克麗絲塔臉上稍微露出吃驚地說道。

「哈哈，有趣的玩笑。我一半同樣是人哦？爽快地叫我娜哈特醬就好」

沒錯，娜哈特剛一說完，烏黑的壓力就朝向了克麗絲塔。
那裡是表情落下了陰影的艾夏一邊用力抱緊娜哈特，一邊投來近於殺氣的視線。

「不，不了。請容我拒絶。那麼，娜哈特小姐，請問把我叫到這裡是有什麼事嗎」

娜哈特慢慢地看了還是老樣子像能面一樣的克麗絲塔的臉。
稍微板起面孔後，娜哈特開口了。

「我要收報酬，你的命和你的同伴們的命的」

娜哈特到底是不會從什麼財產都沒有的奴隷們身上徵收什麼。
但是，他們是一流的冒険者。
既然作為專家在工作就向他們要求回報吧。

「我了解了。我會盡可能地滿足。別看這樣我也是Ａ級。貴族級別的個人財產是有的」

克麗絲塔二話沒說就接受了娜哈特的要求。哪裡都沒有拒絶的理由，也不是會拒絶那樣的忘恩負義。可是，克麗絲塔想不出到底把什麼作為報酬給有力量、有錢財、有房子、有住處、有美貌的娜哈特好。

「我不需要錢呀。啊，不，假如為了進入城鎮需要錢的話那個請你付，但我不需要大量的金錢。我對你們要求的報酬有兩個」

這個世界的貨幣就算入手了對娜哈特來說也幾乎沒有用處。
娜哈特一邊翹二郎腿，一邊對克麗絲塔說。

「一個是在這次救出的奴隷們能獨立生活之前進行最低限度的支援。另一個是在街上傳播我是打倒了Ａ級冒険者的你毫無辦法的對手杜蘭的少女這一傳聞」

前半只是多管閑事。
不覺得被盜賊抓住過的女人們能正經地活下去。那裡一定需要為了重返社會的支援吧。難得娜哈特都幫忙救了她們的命。因為無聊的事死了的話會過意不去。

第二個是為了回避無益的多事而採取的一招。
娜哈特有光走路就會吸引人的魅力。無論怎麼說外表是超絶美少女，當然會受到多管閑事吧。
那樣的話乾脆極力宣傳。
讓周圍知道娜哈特擁有強大的力量。

最初大概也會有以為是戲言而接近來的傻瓜，但是揍個一組、兩組的話，周圍也會控制愚蠢的舉止了吧。
娜哈特不會偽裝自己。
那是否定娜哈特自身的行為，相川徹已經決定作為娜哈特盡全力活著。縱然那會產生糾紛，也不幹在虛偽之中活下去的事。

「只有這些嗎？」

對克麗絲塔來說那作為報酬不成立。

首先，Ａ級冒険者隊伍的命價錢沒那麼便宜。一旦成為了Ａ級，他們一年就能賺到村單位的人能悠閑地度過一生的錢。那樣的話，關於第一個克麗絲塔她們就是費事的程度，什麼東西都不會失去。

關於第二個是論外吧。冒険者基本上負有對委託的報告義務。杜蘭的事也好，娜哈特的事也好，既然打算說明委託失敗還有娜哈特應該領取那個報酬了，傳聞什麼的轉眼就會傳開。

但是，對娜哈特來說那些就足夠了。
相反，因為沒有什麼其他想要的東西。

「嘛，不用那麼緊張。不過，如果你說那作為等價不相稱的話，我就再要求一個吧」

娜哈特偷偷地將視線轉向艾夏。
從不可思議似的歪著頭的艾夏身上移開視線後，再次注視克麗絲塔。

「我想要艾夏母親的情報。名字我記得，叫芙蘿莉婭吧」
「！娜哈特大人，那個⋯⋯」

染上驚愕的艾夏以有點強烈的視線看了娜哈特。

「森精靈的女性，一定是和艾夏很像的美人吧。在你吃得開的範圍內就行。有情報的話就告知我」

對娜哈特的話克麗絲塔點了下頭。
當然，儘管如此還是覺得作為回報不夠，但既然娜哈特又說了一個，克麗絲塔就沒有再說出不滿了。

「我了解了。我以我的名字與你約定吧」

克麗絲塔從移動房屋出去後，龐大的空間裡只剩下娜哈特和艾夏。
繁星流逝。
僅有那樣的安靜的光輝照亮了那個地方。
娜哈特能清楚地看清映在黑夜上的艾夏的臉。

「不滿嗎？」
「⋯⋯不，怎麼會⋯⋯⋯⋯」

儘管嘴上那樣說，但艾夏看起來非常不滿。
她好像幾乎不記得母親的臉。
懂事的時候就是一直和父親兩個人生活。

「討厭母親嗎？」
「⋯⋯⋯⋯是」

艾夏有著是被母親拋棄了的認識。
那是當然的吧。
傷心的時候，難受的時候，作為同樣繼承森精靈之血的人而最希望在身旁的人不在的艾夏的悲傷並非不能理解。

「但是，有活著的可能性。有能遇見骨肉親人的可能性。對我來說那是不存在的──令人羨慕的東西──」

那是曾是相川徹時的記憶的殘渣。
一個人生活上大學的徹最終什麼都沒對雙親說就死了。
和她同樣，什麼都沒能向父親報恩。
什麼都沒能傳達給雙親，也什麼都沒能為他們做。不如說，比雙親更早死了一定是最大級的不孝。
不知是不是那樣的思考浮現了，娜哈特的表情稍微有些暗淡了。

「娜哈特大人⋯⋯⋯⋯」

但是，她還有骨肉親人。
而且是她的父親愛的女人。那樣的話，她只是不負責任地把艾夏丟在了一邊是根本不可能的吧。

「有不滿的話見到之後說就好。有意見的話見到之後吵架就好。在經歷前就否定太可惜了哦，艾夏。──嘛不過，我也明白你方方面面很苦惱。沒什麼，時間有的是。慢慢整理就好」
「⋯⋯⋯⋯是」

娜哈特偷偷地注視艾夏。
來這個世界後得到的重要的羈絆。
一定正因為能和她相遇，徹才作為娜哈特前進了吧。

「艾夏，一起睡嗎」

娜哈特嘴角露出微笑地注視著臉通紅慌張起來的艾夏的身姿。


◇

自由交易都市的中央區。

喧囂無止，來往的人很多的那裡也是別名被稱為中立區的，冒険者公會等公共設施大量集中的地方。
在格外巨大的建築物的最深處，公會長的辦公室裡，有個正難辦似的絞盡腦汁的男人。

「您看怎麼樣，公會長」

戴眼鏡的少女說道。

「⋯⋯⋯⋯姆」

但是，對此沒有明確的答案的冒険者公會長──尼古爾多・哈烏爾表情沉痛地低下頭。

「您看怎麼樣，公會長」
「⋯⋯咕努努努努」

無論怎麼凝視擺在手邊的無數的報告書，也不可能想出好主意。

「快決定，臭老頭！」

戴眼鏡的少女，伊莉娜毫不留情地舉起手上拿著的書的角，完全沒有慈悲地摔下。

「好痛，好痛啊，饒了老夫吧！話說回來，老夫好歹也是公會長啊，很偉大的啊！可你這傢伙卻──」

尼古爾多沒能繼續說下去。
因為毫不留情地被舉起的書一晃露出了凶光。

（插圖００３）

「老夫知道了，是老夫錯了。所以原諒老夫吧！不，請原諒老夫！」
「那麼，您看怎麼樣。Ａ級冒険者冰帝克麗絲塔・尼潔・布蘭里赫特承接盜賊討伐的委託後，一周過去了現在還沒回來。另外，收到了許多在約爾諾森林裡的魔物活躍化了的報告。新人冒険者中已經出現了七名被害者，被報告出現在森林裡的魔物也有複數只高位的鬼（Ogre）、腐魔女（Lich）之類的」

還留有給人年幼的印象的伊莉娜一邊用手臂支撐承受不起的果實，一邊冷靜地對尼古爾多說道。

「前者再過一段時間再說，老夫相信Ａ級冒険者。克麗絲塔總是很冷靜。即使麻煩發生了，她也會靠自身的力量渡過的吧。再過三天還沒回來的話，就讓Ｂ級冒険者作為斥候調查吧。然後森林的事⋯⋯⋯⋯你怎麼看？」

尼古爾多將一流冒険者也會戰慄的銳利視線轉向了伊莉娜。
不過，完全沒有心神不定的樣子的伊莉娜只是陳述了冷靜的意見。

「能想到的可能性有兩個。一個可能性是出現了強大的魔物個體，深層的魔物失去了生存的場所。另一個可能性是，魔素（瑪那）由於某些原因而積存，將其吸收了的個體的活動活躍化了」
「呋～姆，如果只是那樣的話，就能想辦法應付了吧。關係不好的那個貴族也會出動私兵吧」
「您有什麼擔心的嗎？」

對那樣的伊莉娜的話，尼古爾多露出了為難的表情。

「只是直覺──但是，有種不好的預感──」

伊莉娜將眼睛藏在眼鏡後。
然後，儘管覺得很荒唐，還是說出了不可能會有的最壊的可能性。

「萬一，億一⋯⋯是古代的封印──泉水的封印被解開了的話⋯⋯⋯⋯」

魔物活躍化了的現狀也能理解了。
被稱為古代魔族的古老的魔族裡據說也有擁有和魔物親和的能力的人。
不過，如今那就只是傳說。
說出來很蠢。

「假如，假如因為某些原因被解開了的話，你會怎麼辦？」
「逃」

伊莉娜即答。

「現在自由交易都市有兩個Ａ級。老夫也是元Ａ級，你也不亞於Ａ級。即便有持有稱號的克麗絲塔在，也一樣嗎？」
「是，我認為那是自殺行為」

古代魔族的逸聞有很多，但最有名的是與龍交鋒的傳說。
實際現存的有與古代魔族戰鬥的經驗的人曾說過『遇到就是死。我是運氣好』

「是嗎⋯⋯⋯⋯首先是森林的調查。讓Ｃ級以上的隊伍探索森林吧。魔物的討伐也是。然後，把豪斯曼叫來。命令他搜查泉水」

接到尼古爾多的命令，伊莉娜匆匆忙忙動了起來。