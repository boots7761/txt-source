「好像是在製作女性型人偶的過程中，自己也變得想打扮成女性了」
「恭子這個名字是改名了吧？還是說是假名？話雖如此，該說是極端呢還是什麼呢⋯⋯」

一邊和５號桑一起打掃家裡，一邊關於主人的性癖進行討論。那個主人，恭子桑好像悶在自己房間裡做著什麼作業。

「盡可能不要說主人的壊話。恭子大人很溫柔所以不太注意，但你要記住對主人基本上是要絶對服從的」
「真的是沒人權呢，人偶⋯⋯」
「因為讓自動人偶有人權反抗人的話，社會會崩壊的」
「也就是說俺們是奴隷嗎⋯⋯」
「這取決於雇主大人。大部分人對我們會有寵物程度的疼愛」
「寵物⋯⋯」

生活艱難的世界啊。因為這是魔法的世界所以聽了吃驚。
不，對人來說是不錯的世界嗎。但是，希望這是對人偶也溫柔的世界。

「說起來，５號桑之前也去出租了吧？」
「欸欸。就像剛才我告訴你的那樣」
「感，感覺怎麼樣？」
「顧客情報是機密事項，所以難以回答」
「做了怎樣的工作請告訴俺啦」
「就像我已經說過好幾次的那樣哦。大部分是家務。和雇主大人對話。雇主大人是單身男性的場合基本都會伴隨性行為」
「⋯⋯是那樣嗎。但是這個，倫理上會怎麼樣呢？」
「倫理上，是指？」
「不那個⋯⋯總感覺像風俗娘一樣不是嗎。所以⋯⋯」
「和人類們的性行為不同，和自動人偶的性行為幾乎沒有性病的危險性，很乾淨。並且，性行為可能的自動人偶被開發並一般普及之後，性犯罪的案件數激減」
「普通的人類們的戀愛好像也會激減⋯⋯小孩子的出生數之類的也在減少吧？」
「確實小孩子的出生數年年在減少，不過因為勞動用・護理用自動人偶普及了很多，所以即使小孩子的數量減少了對高齡化社會也能充分對應」

不是那種問題啊。那樣的話，人類不斷減少後，不是會變成全是人偶的社會嗎。
俺覺得如果被人偶發起叛亂的話，人類會沒有謀生之道。啊啊，所以人偶對人絶對服從呢。

「⋯⋯違逆委托人的命令的時候，果然還是會被初始化吧？」
「你又說了奇怪的話呢。主人告訴過你吧。我們無法違逆命令。因為被優先度（Ｐ）制御（Ｃ）魔法（Ｃ）制御著」
「那個是讓自動人偶不做不能做的事的制御魔法吧？可是，好像名字不合啊。『優先度』是什麼呢？」
「因為優先度，Priority就是為了遵守自動人偶原則必不可少的東西」
「那是什麼意思呢？」
「比如，我們自動人偶不能採取傷害自身的行動。因為被優先度（Ｐ）制御（Ｃ）魔法（Ｃ）制御著要保護自己。可是，如果是強盜闖進家裡，房主被用槍瞄準著的狀況，自動人偶應該怎麼做呢？」
「那當然必須挺身而出庇護房主吧」
「沒錯。這種場合下比起保護自己的原則，保護人的原則更優先。如果優先度（Ｐ）制御（Ｃ）魔法（Ｃ）不存在的話，自動人偶會變得做不出正確的行動」
「嘿欸。就是行動的優先等級很重要嗎」
「是的。我們因為優先度（Ｐ）制御（Ｃ）魔法（Ｃ），才能一邊遵守自動人偶原則，一邊正確做完雇主大人的命令。領受命令對自動人偶來說是無上的喜悅，是自動人偶的存在理由。所以，不能考慮違逆命令的事哦」

她有著簡直就像天使一樣的內心呢，俺如此諷刺地想到。實際上５號桑是真的那樣認為的吧。
她說俺也要成為天使。
雖然覺得那是亂來啦，但那天總之把５號桑交給的工作完全沒有頂撞地拚命做完了。
因為俺覺得本來淺田悠月就是被當作礙事的人對待的，所以在這裡不努力的話俺的存在就會被消去了。
俺工作是為了自己的保身。
俺想俺是成不了天使的。

＊＊＊＊＊