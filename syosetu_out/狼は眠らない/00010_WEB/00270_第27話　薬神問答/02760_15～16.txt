１５

領主克里姆斯・烏魯邦一直站在領主館的玄關。

藥聖訪問團一行已經抵達西門，要離開城鎮了吧。
特斯拉隊長率領著十位沃卡的守護隊員，會送一行人到最近的村子。

領主必須留在這裡，等待貴族四家當家，以及目送的人們相互問候離去。

「諾瑪，艾達，回去吧」
「啊啊。回去吧」
「嗯。回去吧」
「阿，雷肯。等一下」

叫住的是領主。

「還有什麼事嗎」
「辛苦你了。受關照了」
「啊啊」
「等等！說了等一下吧！」
「有事就快說」
「來了和希拉大人會面，不，謁見的申請」
「這種事誰知道」
「因為沒傳達給你。斯卡拉貝爾導師滯留時，所有的局外者都不準進入。因此，接下來會變忙」
「就說誰知道了」
「雖然還要一點時間，但竟然連邦塔羅伊領主都申請了沃卡訪問。那個邦塔羅伊領主親自出場喔，說是要過來這裡！」
「太好了呢。我走了」
「別回去！等一下」
「所以有事就快說」
「想請希拉大人在迎賓館待一會」
「去跟希拉說」
「希拉大人去哪裡了？」
「誰知道」
「你沒可能不知道吧」
「沒看到嗎？希拉消失了」
「啊啊，消失了呢。真厲害的魔法。那是魔法吧？」
「應該是吧。推測不出是什麼魔法就是了」
「去哪裡了」
「這問題剛剛回答了」
「真的不知道嗎」
「真的不知道」
「什麼時候會回來？」
「不知道」
「你覺得老夫會接受那種答案嗎」
「不管你接不接受，都跟我無關」
「謁見的申請，已經來好幾個了。也有不得不交付的書簡。在這之後，會有成山多的謁見申請吧。會送來很多書簡吧」
「能讓我給個助言嗎」
「務必拜託了」
「希拉不會再出現於這城鎮了。所以會面就拒絶吧。書簡由你來寫回覆。就算放在希拉家裡，也只會變垃圾」
「開玩笑的吧。拜託說這是在開玩笑」
「囉嗦。希拉不會再現身了。對這件事感到最遺憾的是我」

雷肯說完，就催促諾瑪和艾達離開領主館。

雖然也覺得領主有點可憐，但這次得到的東西應該多上很多才對，所以也有想叫他為善後這點事苦惱的心情。
在途中跟諾瑪分別，和艾達一起迎接傑利可，然後回了家。


１６

希拉不會再出現於這城鎮了，雷肯的這句話，似乎讓艾達受到衝擊，便詢問了雷肯，希拉到底去哪裡了。

雷肯回答了。
受人注目和擾亂，是希拉最討厭的事，既然名聲傳得這麼廣了，就不會再出現於這城鎮了吧。

希拉去哪裡了，自己也不知道。但是，希拉絶不會捨棄雷肯和艾達。
總有一天，希拉會在自己面前現身吧。
到時候，自己和艾達，得成長得出色到能讓她感到高興才行。

而且畢竟是那個希拉，就算不現身，想必也會守望著自己和艾達。
把傑利可寄託給艾達，就近似於再會的約定。
聽了雷肯的這些話，艾達擦了眼淚，嗯，地點了頭。

下一天，雷肯沒有外出。
緊閉在家裡，邊靜靜地喝酒，邊謹慎地以〈生命感知〉探查城鎮的情況。
但是，沒有期待的反應。
下一天也一樣。
在第三天的夜晚，等待許久的反應出現了。強大魔力的氣息。懷念的氣息。
那氣息馬上就消失了。但場所有記住。

雷肯對自己施展〈隱蔽〉消除氣息，在夜晚的沃卡城，向西北移動。在圍牆上奔跑，飛越屋頂，抵達了那場所。
清靜的住宅街裡的小而俱全的家。
內側有庭院，面向庭院的二樓有陽台，老女看著夜空，喝著茶。
雷肯輕巧地降到那陽台上。

「呀。真快呢。那邊有你的茶喔」
「那打扮挺時髦的阿」
「在這裡會用這打扮」
「何時開始的」
「我想想。有十年左右吧」
「這裡沒有大煙囪呢。也沒有地下室」
「沒有呢。藥已經夠了吧。也培育了一些後繼者了」
「你批發藥的藥屋會很難過吧」
「也不會喔。現在這城鎮裡也有好幾個優秀的藥師。而且有把感冒藥和萬能藥的作法，當作禮物送給有在來往的五家藥屋喔。應該很高興吧」
「是嗎」

雷肯坐下來喝了茶。

「真好喝的茶。還能來這裡喝這茶嗎？」
「偶爾的話」

眺望著夜空好一陣子。

「你阿。今年過後會離開城鎮吧？」
「啊啊」
「小艾達要怎麼辦？」
「我在迷惘。但是，覺得放她自己一個人一次也不錯」
「這樣阿」
「妳怎麼想？」
「也是呢。小艾達自己，大概也想跟你分開，考驗看看自己不是嗎」
「是嗎」
「然後，總有一天想讓自己變得能讓你來迎接她。我是這麼看的」

這賢女，不會毫無根據說這種話。
也就是說，有和艾達進行過，會讓她這麼感覺的對話。

「我也覺得那樣比較好」
「這對你來說也是好事喔。不用擔心那孩子。有傑利可在的」
「有傑利可在就能安心了呢」
「要是發生了什麼，傑利可會通知我的」
「原來如此」

看著只剩一點的茶杯底，雷肯突然想起來了。

「這麼說來，羅蘭打算使用魔結界的那一晚，你說了，就算用迷宮深層的魔石來張開魔結界，用恩寵品拉高實力來施展〈睡眠〉，也不會對艾達和我有效」
「有說呢」
「那很奇怪」
「嘿？」
「的確，艾達對精神系魔法有抵抗力吧。生命力的總量也不少吧。但我不覺得僅憑這些能防住羅蘭用上魔結界和恩寵品來下的〈睡眠〉。更何況我當時沒有戴〈因邱雅多羅之首飾〉」

隨時戴在手指上的銀色戒指，雖然會提升狀態異常的耐性，但那效果有限。實際上，馬拉奇斯的魔法也削掉了雷肯的意識一瞬間。

「那個，是對德魯斯坦那小少爺的牽制阿」
「什麼？」
「不能對那小少爺大意喔」

雷肯也有注意到，藥聖訪問團中，與之為敵會最麻煩的是神殿騎士德魯斯坦・瓦爾莫。這次，雖然看起來忠實地完成了斯卡拉貝爾的護衛這職務，但那男人放在第一的應該是王都艾雷克斯神殿的利益。那不一定都會跟希拉和雷肯的利害一致。那男人一邊擺著張笑臉，一邊冷靜地觀察著希拉、雷肯、艾達和諾瑪。

也就是說，那對話是希拉在警告德魯斯坦，別對雷肯和艾達出手。
這麼說來，當時希拉暗示了自己與鴉庫魯班多有很深的關聯。要與希拉為敵需要相當大的覺悟，德魯斯坦應該會這麼想。

「原來如此。理解了。還有其他巢穴嗎？」
「你說什麼？」
「你以前說過，狡兔三窟。也說過在這国家有在好幾個城鎮裡確保據點」
「有說過這種事阿」
「有說過。被神殿傳喚的時候」
「你記憶力意外得不錯呢」
「那麼，其他巢穴在哪裡」
「誰知道呢。年紀大了就會忘掉各種事情。挖了幾個巢穴，在哪裡挖了巢穴，這種事可不會特地記著喔。不過必要的時候就會想起來吧」

（最不能大意的是這老婆婆阿）

雷肯把茶飲盡。

「打擾了」
「我家裡的調藥道具有想要的就拿吧。保重阿」
「啊啊。謝謝」

雷肯再次對自己施展〈隱蔽〉，在夜晚的城鎮飛躍。

是美麗星星之夜。
雷肯一邊飛簷走壁，一邊跳著舞。
旋轉身體，把腳抬高，揮舞手臂，跳出後空翻，與星星一同舞動。
是情不自禁想跳舞的夜晚。

不知道該如何表現的心情，從胸口溢出，實在沒辦法僵著不動。

寒風凜凜。
風之寒冷非常暢快。