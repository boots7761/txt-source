非常感謝各位看完《加速世界》第２２集〈絶焰太陽神〉。

從上一集《冰雪妖精》算起，讓大家等了足足十一個月，真的非常抱歉。二〇一七年發生了很多事，不得不把資源灌注在其他系列，但即使是這樣，還是間隔太久了呢。我會努力盡快把下一集呈現在各位讀者面前！

針對本書內容也稍微提一下。記得我在上一集的後記裡，寫說「下次想寫悠哉點的日常故事」。可是結果一揭曉，卻變成不太悠哉的故事⋯⋯這陣子劇情的比重很容易偏向加速世界內，所以我一直希望對於春雪他們的現實人生，也能好好描寫一番，但世界情勢卻一直不容我這麼做。試著將到本集為止的二十二集照篇章來劃分，我想應該會是這樣：１、２集是發動篇、３、４集是掠奪者篇、５～９集是災禍之鎧篇、第１０集是短篇集，１１～１６集是ISS套件篇，１７集以後是白之團篇。但這樣一列下來，這２２集也已經進入了全劇大高潮的部分，要把事件做個了結，回到日常生活，似乎還得花上一些時間。只是話說回來，春雪已經和黑雪姫還有仁子等人約好，要去山形縣的外公家玩，而且也得和班級委員長生澤同學一起參加學生會幹部選舉⋯⋯站在我的立場，也是希望在下一集就可以和白之團做個了結！我是這麼想的！

那麼那麼，如果各位讀者看看本書日文版的封面書背最上面，應該會看到上面印有「か─１６─５０」。「か─１６」表示這是在電擊文庫出版的，筆名以「か」開頭的第十六名作家，「５０」則是該作家所出的第幾本書。也就是說，這本《加速世界》第２２集，是我的第五十本書。

我的出道作《加速世界》第１集（編號當然是「か─１６─１」）出版，是在二〇〇九年二月，所以走到這一步，算來花了八年又九個月。回顧起來就覺得看似很長，卻又轉眼間就過去，但不管怎麼說，能夠累積到五十本這個數字，全都是拜支持我到今天的各位讀者所賜。下一個里程碑，應該會是一百本，儘管還不確定寫不寫得到，但我今後也會朝這個目標繼續努力，還請各位讀者繼續給予支持與愛護。

明明空了將近一整年，這次卻還是拖到最後關頭，被我添了很多麻煩的插畫師HIMA老師、責任編輯三木先生、安達，也非常感謝各位。我打算下一集無論出書間隔還是劇情進展，都要加快腳步！我會努力的！

二〇一七年十月某日 川原 礫